package lesson3_streams_and_lambdas;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * esta clase no esta en la pag del curso asi que no tenia una guia para saber que debia hacer ya que el pdf de los enunciados toma en cuenta esta clase como si estuviera presente en las decargar.
 * al final las respuestar terminaron siendo casi copiadas en su totalidad.
 * no estoy seguro si esa era la intencion o fue un error ya que el metodo allWords no es usado en ningun momento.
 */
public class RandomWords {
	private final List<String>sourceWords;

	public RandomWords() throws IOException{
		try(BufferedReader reader = Files.newBufferedReader(Paths.get("words"))){
			sourceWords = reader.lines().collect(Collectors.toList());
		      System.out.println("Loaded " + sourceWords.size() + " words");
			
		}
	}
	
	public List<String> createList(int listSize){
		Random random = new Random();
		
		List<String>randomWords = random.ints(listSize, 1, sourceWords.size())
					 					.mapToObj(sourceWords::get)
					 					.collect(Collectors.toList());
		return randomWords;
	}
	
	//public List<String> allWords(){	}

}
